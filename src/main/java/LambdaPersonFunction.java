import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by user on 17.02.2017.
 */
public class LambdaPersonFunction {
    public static void main(String[] args) {

        BiFunction<Person, String, Boolean> personFirstNameEquals = (person, string) -> person.getFirstName().equals(string);

        BiFunction<Person, String, Boolean> personLastNameEquals = (person, string) -> person.getLastName().equals(string);

        BiFunction<Person, Integer, Boolean> personAgeEquals = (person, age) -> person.getAge().equals(age);

        BiFunction<Person, Integer, Boolean> personAgeGreaterThan = (person, age) -> person.getAge() > age;

        BiFunction<Person, Integer, Boolean> personAgeLessThan = (person, age) -> person.getAge() < age;

        BiFunction<Person, Integer, Boolean> personAgeGreaterThanOrEqual = (person, age) -> person.getAge() >= age;

        BiFunction<Person, Integer, Boolean> personAgeLessThanOrEqual = (person, age) -> person.getAge() <= age;

        BiFunction<Person, Gender, Boolean> personGenderEquals = (person, gender) -> person.getGender().equals(gender);

        BiFunction<Person, Integer, Person> personSetAge = (person, age) -> new Person(person).setAge(age);

        BiFunction<Person, String, Person> personSetFirstName = (person, string) -> new Person(person).setFirstName(string);

        BiFunction<Person, String, Person> personSetLastName = (person, string) -> new Person(person).setLastName(string );

        BiFunction<Person, Gender, Person> personSetGender = (person, gender) -> new Person(person).setGender(gender);

        BiFunction<List<Person>, Predicate<Person>, List<Person>> filter = (personList, personBooleanFunction) -> personList.stream().filter(personBooleanFunction).collect(Collectors.toList());

        Predicate<Person> isAdult = person -> person.getAge() >= 18;

        Predicate<Person> isMale = person -> person.getGender().equals(Gender.MALE);

        Predicate<Person> isAdultMale = isAdult.and(isMale);
        System.out.println("List unfiltered:");
        List<Person> perList = new ArrayList<>();
        perList.add(new Person("Smith", "Agent", 42, Gender.MALE));
        perList.add(new Person("Trinity", "Neovna", 33, Gender.FEMALE));
        perList.add(new Person( "Neo", "Matrixov", 30, Gender.MALE));
        perList.add(new Person( "Morfeus", "Matrixov", 51, Gender.MALE));

        perList.stream().forEach(System.out::println);

        List<Person> filteredPersonList = filter.apply(perList, isAdultMale);

        System.out.println("List filtered:");
        filteredPersonList.stream()
                .forEach(System.out::println);
    }
}

